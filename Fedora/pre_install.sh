#!/bin/bash

mkdir -p /mnt/FernandoHCorrea/Backup
mkdir -p /mnt/FernandoHCorrea/CloudDrive
mkdir -p /mnt/FernandoHCorrea/Backup/Downloads
chown fcorrea:fcorrea /mnt/FernandoHCorrea/ -R

mkdir -p /home/projects/pessoal
mkdir -p /home/projects/profissional
chown fcorrea:fcorrea /home/projects/ -R
